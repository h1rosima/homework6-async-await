document.addEventListener('DOMContentLoaded', () => {
    document.body.style.display = 'flex';
    document.body.style.flexDirection = 'column';
    document.body.style.alignItems = 'center';
    const button = document.getElementById('button');
    button.style.width = '300px';
    button.style.height = '100px';
    button.style.margin = '300px 0 0 0';

    button.addEventListener('click', IP);
});

async function IP() {
    const list = document.getElementById('div');
    try {
        const ipResponse = await fetch('https://api.ipify.org/?format=json');
        if (!ipResponse.ok) {
            throw new Error(`Error fetching IP: ${ipResponse.statusText}`);
        }
        const ipData = await ipResponse.json();
        const ip = ipData.ip;
        console.log(ipData);

        const SendResponse = await fetch(`http://ip-api.com/json/${ip}`);
        if (!SendResponse.ok) {
            throw new Error(
                `Network response was not ok: ${SendResponse.statusText}`
            );
        }
        const data = await SendResponse.json();
        console.log(data);

        list.style.fontSize = '20px';

        list.innerHTML = `
            <p>Статус: ${data.status}</p>
            <p>Страна: ${data.country}</p>
            <p>Регион: ${data.regionName}</p>
            <p>Город: ${data.city}</p>
            <p>Время: ${data.timezone}</p>
        `;
    } catch (error) {
        console.error('Error:', error);
        alert(`Error: ${error.message}`);
    }
}
